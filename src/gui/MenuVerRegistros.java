package gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuVerRegistros extends JDialog {
	CuadroRegistro frmregcuadro = new CuadroRegistro();
	EstudioRegistro frmestcuadro = new EstudioRegistro();
	PlanRegistro frmplancuadro = new PlanRegistro();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MenuVerRegistros dialog = new MenuVerRegistros();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MenuVerRegistros() {
		setBounds(100, 100, 443, 403);
		getContentPane().setLayout(null);
		
		JLabel lblRegistros = new JLabel("Registros");
		lblRegistros.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistros.setFont(new Font("Segoe Print", Font.PLAIN, 18));
		lblRegistros.setBounds(158, 11, 107, 58);
		getContentPane().add(lblRegistros);
		
		JButton btnCuadroDeNecesidades = new JButton("Cuadro de Necesidades");
		btnCuadroDeNecesidades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmregcuadro.setVisible(true );
			}
		});
		btnCuadroDeNecesidades.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnCuadroDeNecesidades.setBackground(SystemColor.controlShadow);
		btnCuadroDeNecesidades.setBounds(128, 69, 177, 78);
		getContentPane().add(btnCuadroDeNecesidades);
		
		JButton btnEstudioDeMercado = new JButton("Estudio de Mercado");
		btnEstudioDeMercado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmestcuadro.setVisible(true);
			}
		});
		btnEstudioDeMercado.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnEstudioDeMercado.setBackground(SystemColor.controlShadow);
		btnEstudioDeMercado.setBounds(128, 169, 177, 70);
		getContentPane().add(btnEstudioDeMercado);
		
		JButton btnPlanAnual = new JButton("Plan Anual");
		btnPlanAnual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmplancuadro.setVisible(true);
			}
		});
		btnPlanAnual.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnPlanAnual.setBackground(SystemColor.controlShadow);
		btnPlanAnual.setBounds(128, 250, 177, 78);
		getContentPane().add(btnPlanAnual);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						btnCancelarCierra(e);
					}
					void btnCancelarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnRegresar.setBackground(SystemColor.controlShadow);
		btnRegresar.setBounds(329, 331, 88, 23);
		getContentPane().add(btnRegresar);
	}
}
