package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.HierarchyListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ScrollPaneConstants;
import javax.swing.JList;
import javax.swing.JTextArea;

import controlador.ControladorCuadro;
import entidad.CuadroBean;

import java.awt.Cursor;
import java.sql.Array;
import java.util.Arrays;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class CuadroNecesidades extends JDialog {

	private JPanel contentPane = new JPanel();
	private JTable table1;
	private JTextField txtFechaPedido;
	private JTextField txtPresupuesto;
	private JTextField txtNumero;
	private JComboBox cboTipo;
	private JTextField txtDescripcion;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			CuadroNecesidades dialog = new CuadroNecesidades();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public CuadroNecesidades() {
		setBounds(100, 100, 604, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCuadroNecesidades = new JLabel("Cuadro de Necesidades");
		lblCuadroNecesidades.setBounds(216, 25, 198, 14);
		contentPane.add(lblCuadroNecesidades);
		
		final JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(40, 50, 46, 14);
		contentPane.add(lblNumero);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(40, 81, 46, 14);
		contentPane.add(lblTipo);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(40, 112, 54, 14);
		contentPane.add(lblDescripcion);
		
		final JTextField txtDescripcion = new JTextField();
		txtDescripcion.setBounds(120, 109, 155, 20);
		contentPane.add(txtDescripcion);
		txtDescripcion.setColumns(10);
		
		final JComboBox cboTipo = new JComboBox();
		cboTipo.setModel(new DefaultComboBoxModel(new String[] {"RRHH", "Servicios", "Productos"}));
		cboTipo.setBounds(120, 78, 155, 20);
		contentPane.add(cboTipo);
		
		JLabel lblFechaDePedido = new JLabel("Fecha Pedido");
		lblFechaDePedido.setBounds(40, 144, 77, 14);
		contentPane.add(lblFechaDePedido);
		
		txtFechaPedido = new JTextField();
		txtFechaPedido.setBounds(120, 140, 155, 20);
		contentPane.add(txtFechaPedido);
		txtFechaPedido.setColumns(10);
		
		JButton btnProcesar = new JButton("Procesar");
		btnProcesar.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		btnProcesar.setBackground(SystemColor.controlShadow);
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//agrega a la Tabla
				DefaultTableModel modelo = (DefaultTableModel) table1.getModel();
				Object [] fila =  new Object[5];
				fila[0] = txtNumero.getText(); 
				fila[1] = cboTipo.getSelectedItem().toString();
				fila[2] = txtDescripcion.getText();
				fila[3] = txtFechaPedido.getText();
				fila[4] = txtPresupuesto.getText();
				
				modelo.addRow(fila);
				table1.setModel(modelo);
				
				
				
				//Pasa la info a la BD 
				CuadroBean bean = new CuadroBean();
				bean.setIdnumero(Integer.parseInt(txtNumero.getText().trim()));
				bean.setTipo((String)cboTipo.getSelectedItem());
				bean.setDescripcion(txtDescripcion.getText().trim());
				//bean.setFecha(txtFechaPedido.getText().trim());
				bean.setPresupuesto(txtPresupuesto.getText().trim());
				
				ControladorCuadro c = new ControladorCuadro();
				 int salida = c.insertaCuadro(bean);
				 if(salida>0){
					 JOptionPane.showMessageDialog(null, "Registro Exitoso");
					 txtNumero.setText("");
						txtDescripcion.setText("");
						txtFechaPedido.setText("");
						txtPresupuesto.setText("");
						
						txtNumero.requestFocus();
				 }else{
					 JOptionPane.showMessageDialog(null, "Error en el Registro");
					 txtNumero.setText("");
						txtDescripcion.setText("");
						txtFechaPedido.setText("");
						txtPresupuesto.setText("");
						
						txtNumero.requestFocus();
				 }
				
			}
		});
		btnProcesar.setBounds(358, 140, 89, 23);
		contentPane.add(btnProcesar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(40, 179, 507, 179);
		contentPane.add(scrollPane);
		
		table1 = new JTable();
		table1.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Numero", "Tipo", "Descripcion", "Fecha", "Presupuesto"
			}
		));
		table1.getColumnModel().getColumn(0).setPreferredWidth(50);
		scrollPane.setViewportView(table1);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		btnRegresar.setBackground(SystemColor.controlShadow);
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						btnRegresarCierra(e);
					}
					void btnRegresarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setBounds(468, 369, 89, 23);
		contentPane.add(btnRegresar);
		
		JLabel lblPresupuesto = new JLabel("Presupuesto");
		lblPresupuesto.setBounds(318, 50, 77, 14);
		contentPane.add(lblPresupuesto);
		
		txtPresupuesto = new JTextField();
		txtPresupuesto.setBounds(416, 47, 131, 20);
		contentPane.add(txtPresupuesto);
		txtPresupuesto.setColumns(10);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		btnEliminar.setBackground(SystemColor.controlShadow);
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table1.getModel();
				int a = table1.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminar.setBounds(458, 140, 89, 23);
		contentPane.add(btnEliminar);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(120, 47, 155, 20);
		contentPane.add(txtNumero);
		txtNumero.setColumns(10);
		
		
	}
}
