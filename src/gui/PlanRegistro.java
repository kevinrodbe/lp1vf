package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PlanRegistro extends JFrame {

	private JPanel contentPane;
	private JTable tableTechos;
	private JTable tableRd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlanRegistro frame = new PlanRegistro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PlanRegistro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 562, 519);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPlanAnual = new JLabel("Plan Anual");
		lblPlanAnual.setFont(new Font("Segoe Script", Font.PLAIN, 15));
		lblPlanAnual.setBounds(224, 11, 111, 38);
		contentPane.add(lblPlanAnual);
		
		JLabel lblTechosPresupuestales = new JLabel("Techos Presupuestales");
		lblTechosPresupuestales.setBounds(10, 59, 162, 14);
		contentPane.add(lblTechosPresupuestales);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(163, 91, 373, 131);
		contentPane.add(scrollPane);
		
		tableTechos = new JTable();
		tableTechos.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Numero", "Fecha", "Descripcion", "Cantidad", "Costo Total"
			}
		));
		tableTechos.getColumnModel().getColumn(0).setPreferredWidth(70);
		tableTechos.getColumnModel().getColumn(1).setPreferredWidth(65);
		tableTechos.getColumnModel().getColumn(2).setPreferredWidth(100);
		tableTechos.getColumnModel().getColumn(3).setPreferredWidth(64);
		tableTechos.getColumnModel().getColumn(4).setPreferredWidth(70);
		scrollPane.setViewportView(tableTechos);
		
		JLabel lblRd = new JLabel("RD");
		lblRd.setBounds(10, 231, 46, 14);
		contentPane.add(lblRd);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 256, 526, 153);
		contentPane.add(scrollPane_1);
		
		tableRd = new JTable();
		tableRd.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Numero", "Fecha", "Descripcion"
			}
		));
		tableRd.getColumnModel().getColumn(2).setPreferredWidth(150);
		scrollPane_1.setViewportView(tableRd);
		
		JButton btnEliminarTecho = new JButton("Eliminar Techo");
		btnEliminarTecho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) tableTechos.getModel();
				int a = tableTechos.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminarTecho.setBounds(10, 101, 130, 38);
		contentPane.add(btnEliminarTecho);
		
		JButton btnBuscarTechos = new JButton("Buscar Techos");
		btnBuscarTechos.setBounds(10, 169, 130, 38);
		contentPane.add(btnBuscarTechos);
		
		JButton btnEliminarRd = new JButton("Eliminar RD");
		btnEliminarRd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) tableRd.getModel();
				int a = tableRd.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminarRd.setBounds(99, 420, 101, 39);
		contentPane.add(btnEliminarRd);
		
		JButton btnBuscarRd = new JButton("Buscar RD");
		btnBuscarRd.setBounds(325, 420, 101, 39);
		contentPane.add(btnBuscarRd);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						btnRegresarCierra(e);
					}
					void btnRegresarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setBounds(447, 447, 89, 23);
		contentPane.add(btnRegresar);
	}

}
