package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Cursor;
import java.awt.Color;
import java.awt.Font;

public class MenuPrincipal extends JDialog {

	private JPanel contentPane = new JPanel();
	CuadroNecesidades frmCuadro = new CuadroNecesidades();
	EstudioMercado frmMercado = new EstudioMercado();
	PlanAnual frmplan = new PlanAnual();
	MenuVerRegistros frmRegistros = new MenuVerRegistros();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MenuPrincipal dialog = new MenuPrincipal();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MenuPrincipal() {
		
		setBounds(100, 100, 293, 331);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBienvenido = new JLabel("Bienvenido \r\nUser1");
		lblBienvenido.setToolTipText("Welcome");
		lblBienvenido.setFont(new Font("Consolas", Font.PLAIN, 20));
		lblBienvenido.setForeground(Color.BLUE);
		lblBienvenido.setBounds(27, 13, 176, 27);
		contentPane.add(lblBienvenido);
		
		JButton btnCuadroNecesidades = new JButton("Cuadro de Necesidades");
		btnCuadroNecesidades.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnCuadroNecesidades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCuadro.setVisible(true);
			}
		});
		btnCuadroNecesidades.setBackground(SystemColor.controlShadow);
		btnCuadroNecesidades.setBounds(27, 51, 228, 50);
		contentPane.add(btnCuadroNecesidades);
		
		JButton btnEstudioMercado = new JButton("Estudio de Mercado");
		btnEstudioMercado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmMercado.setVisible(true);
			}
		});
		btnEstudioMercado.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnEstudioMercado.setBackground(SystemColor.controlShadow);
		btnEstudioMercado.setBounds(27, 112, 228, 50);
		contentPane.add(btnEstudioMercado);
		/*
		JButton btnPlanAnual = new JButton("Plan Anual");
		btnPlanAnual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmplan.setVisible(true);
			}
		});
		btnPlanAnual.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnPlanAnual.setBackground(SystemColor.controlShadow);
		btnPlanAnual.setBounds(27, 173, 228, 50);
		contentPane.add(btnPlanAnual);
		*/
		JButton btnRegistros = new JButton("Ver Registros");
		btnRegistros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmRegistros.setVisible(true);
			}
		});
		btnRegistros.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnRegistros.setBackground(SystemColor.controlShadow);
		btnRegistros.setBounds(27, 234, 228, 48);
		contentPane.add(btnRegistros);
	}
}
