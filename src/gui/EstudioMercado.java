package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JSplitPane;
import javax.swing.BoxLayout;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JSpinner;
import javax.swing.JSeparator;

import java.awt.Choice;

import javax.swing.JFormattedTextField;
import javax.swing.Box;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorEmpresa;
import entidad.EmpresaBean;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class EstudioMercado extends JDialog {
	private JTextField txtRuc;
	private JTextField txtNombreEmpresa;
	private JTextField txtNumero;
	private JTextField txtDescripcion;
	private JTextField txtPrecio;
	private JTextField txtCantidad;
	private JTextField txtFecha;
	private JTable table2;
	private JComboBox cboTipo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EstudioMercado dialog = new EstudioMercado();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public EstudioMercado() {
		setBounds(100, 100, 514, 589);
		getContentPane().setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 158, 478, 12);
		getContentPane().add(separator);
		
		JLabel lblRuc = new JLabel("RUC");
		lblRuc.setBounds(10, 86, 46, 14);
		getContentPane().add(lblRuc);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 123, 46, 14);
		getContentPane().add(lblNombre);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(304, 86, 46, 14);
		getContentPane().add(lblTipo);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnBuscar.setBounds(304, 123, 89, 23);
		getContentPane().add(btnBuscar);
		
		JButton btnAgregarEmpresa = new JButton("Agregar");
		btnAgregarEmpresa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Pasa Nueva Empresa a la BD
				EmpresaBean bean = new EmpresaBean();
				bean.setRuc(txtRuc.getText().trim());
				bean.setNombre(txtNombreEmpresa.getText().trim());
				bean.setTipo("Productos");
				System.out.println(bean.getTipo());
				//bean.setTipo(cboTipo.getSelectedIndex());
			
				
				ControladorEmpresa c = new ControladorEmpresa();
				 int salida = c.insertaEmpresa(bean);
				 if(salida>0){
					 JOptionPane.showMessageDialog(null, "Registro Exitoso");
				 }else{
					 JOptionPane.showMessageDialog(null, "Error en el Registro");
				 }
			}
		});
		btnAgregarEmpresa.setBounds(399, 124, 89, 23);
		getContentPane().add(btnAgregarEmpresa);
		
		txtRuc = new JTextField();
		txtRuc.setBounds(72, 86, 157, 20);
		getContentPane().add(txtRuc);
		txtRuc.setColumns(10);
		
		txtNombreEmpresa = new JTextField();
		txtNombreEmpresa.setBounds(72, 120, 157, 20);
		getContentPane().add(txtNombreEmpresa);
		txtNombreEmpresa.setColumns(10);
		
		JComboBox cboTipo = new JComboBox();
		cboTipo.setModel(new DefaultComboBoxModel(new String[] {"Servicios", "Productos"}));
		cboTipo.setBounds(376, 83, 112, 20);
		getContentPane().add(cboTipo);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(10, 201, 46, 14);
		getContentPane().add(lblNumero);
		
		JLabel lblCategoria = new JLabel("Categoria");
		lblCategoria.setBounds(10, 241, 69, 14);
		getContentPane().add(lblCategoria);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(10, 277, 69, 14);
		getContentPane().add(lblDescripcion);
		
		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(304, 201, 46, 14);
		getContentPane().add(lblPrecio);
		
		JLabel lblCantidad = new JLabel("Cantidad");
		lblCantidad.setBounds(304, 241, 62, 14);
		getContentPane().add(lblCantidad);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(304, 277, 46, 14);
		getContentPane().add(lblFecha);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(93, 198, 136, 20);
		getContentPane().add(txtNumero);
		txtNumero.setColumns(10);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setBounds(93, 274, 136, 20);
		getContentPane().add(txtDescripcion);
		txtDescripcion.setColumns(10);
		
		txtPrecio = new JTextField();
		txtPrecio.setBounds(376, 198, 112, 20);
		getContentPane().add(txtPrecio);
		txtPrecio.setColumns(10);
		
		txtCantidad = new JTextField();
		txtCantidad.setBounds(376, 238, 112, 20);
		getContentPane().add(txtCantidad);
		txtCantidad.setColumns(10);
		
		txtFecha = new JTextField();
		txtFecha.setBounds(376, 274, 112, 20);
		getContentPane().add(txtFecha);
		txtFecha.setColumns(10);
		
		final JComboBox cboCategoria = new JComboBox();
		cboCategoria.setModel(new DefaultComboBoxModel(new String[] {"Servicio", "Producto"}));
		cboCategoria.setBounds(93, 238, 136, 20);
		getContentPane().add(cboCategoria);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 302, 478, 204);
		getContentPane().add(scrollPane);
		
		table2 = new JTable();
		table2.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Numero", "Categoria", "Descripcion", "RUC Empresa", "Precio", "Cantidad", "Fecha"
			}
		));
		table2.getColumnModel().getColumn(0).setPreferredWidth(56);
		table2.getColumnModel().getColumn(1).setPreferredWidth(71);
		table2.getColumnModel().getColumn(3).setPreferredWidth(88);
		table2.getColumnModel().getColumn(4).setPreferredWidth(50);
		table2.getColumnModel().getColumn(5).setPreferredWidth(60);
		table2.getColumnModel().getColumn(6).setPreferredWidth(54);
		scrollPane.setViewportView(table2);
		
		JButton btnProcesar = new JButton("Procesar");
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel modelo = (DefaultTableModel) table2.getModel();
				Object [] fila =  new Object[7];
				fila[0] = txtNumero.getText(); 
				fila[1] = cboCategoria.getSelectedItem().toString();
				fila[2] = txtDescripcion.getText();
				fila[3] = txtRuc.getText();
				fila[4] = txtPrecio.getText();
				fila[5] = txtCantidad.getText();
				fila[6] = txtFecha.getText();
				modelo.addRow(fila);
				table2.setModel(modelo);
				//Agrega Lo de la tabla a la BD
				txtNumero.setText("");
				txtDescripcion.setText("");
				txtRuc.setText("");
				txtPrecio.setText("");
				txtCantidad.setText("");
				txtFecha.setText("");
				txtNumero.requestFocus();
			}
		});
		btnProcesar.setBounds(39, 517, 89, 23);
		getContentPane().add(btnProcesar);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						btnCancelarCierra(e);
					}
					void btnCancelarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setBounds(399, 517, 89, 23);
		getContentPane().add(btnRegresar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table2.getModel();
				int a = table2.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminar.setBounds(213, 517, 89, 23);
		getContentPane().add(btnEliminar);
		
		JLabel lblCotizacin = new JLabel("Cotizaci\u00F3n");
		lblCotizacin.setForeground(Color.BLUE);
		lblCotizacin.setFont(new Font("Consolas", Font.PLAIN, 18));
		lblCotizacin.setBounds(10, 162, 136, 28);
		getContentPane().add(lblCotizacin);
		
		JLabel lblEmpresa = new JLabel("Empresa");
		lblEmpresa.setForeground(Color.BLUE);
		lblEmpresa.setFont(new Font("Consolas", Font.PLAIN, 18));
		lblEmpresa.setBounds(10, 47, 136, 28);
		getContentPane().add(lblEmpresa);
		
		JLabel lblEstudioDeMercado = new JLabel("Estudio de Mercado");
		lblEstudioDeMercado.setForeground(Color.BLUE);
		lblEstudioDeMercado.setFont(new Font("Consolas", Font.PLAIN, 18));
		lblEstudioDeMercado.setBounds(147, 11, 203, 28);
		getContentPane().add(lblEstudioDeMercado);
	}
}
