package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CuadroRegistro extends JFrame {

	private JPanel contentPane;
	private JTable tableCuadro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuadroRegistro frame = new CuadroRegistro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CuadroRegistro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 532);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCuadroDeNecesidades = new JLabel("Cuadro de Necesidades");
		lblCuadroDeNecesidades.setFont(new Font("Segoe Script", Font.PLAIN, 16));
		lblCuadroDeNecesidades.setBounds(189, 42, 208, 47);
		contentPane.add(lblCuadroDeNecesidades);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(35, 113, 505, 288);
		contentPane.add(scrollPane);
		
		tableCuadro = new JTable();
		tableCuadro.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Numero", "Tipo", "Descripcion", "Fecha", "Presupuesto"
			}
		));
		tableCuadro.getColumnModel().getColumn(0).setPreferredWidth(59);
		tableCuadro.getColumnModel().getColumn(1).setPreferredWidth(88);
		tableCuadro.getColumnModel().getColumn(2).setPreferredWidth(111);
		tableCuadro.getColumnModel().getColumn(3).setPreferredWidth(60);
		scrollPane.setViewportView(tableCuadro);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel model = (DefaultTableModel) tableCuadro.getModel();
				int a = tableCuadro.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminar.setBackground(SystemColor.controlShadow);
		btnEliminar.setBounds(35, 448, 89, 23);
		contentPane.add(btnEliminar);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						btnRegresarCierra(e);
					}
					void btnRegresarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setBackground(SystemColor.controlShadow);
		btnRegresar.setBounds(451, 448, 89, 23);
		contentPane.add(btnRegresar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBackground(SystemColor.controlShadow);
		btnBuscar.setBounds(252, 448, 89, 23);
		contentPane.add(btnBuscar);
	}
}
