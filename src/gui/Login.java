package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.SystemColor;

import javax.swing.UIManager;

import java.awt.Color;

import javax.swing.border.MatteBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtId;
	Registro frmRegistro = new Registro();
	MenuPrincipal frmPrincipal = new MenuPrincipal();
	private JPasswordField txtContrase�a;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		
		setBounds(100, 100, 343, 344);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIngresarUsuario = new JLabel("Ingresar Usuario");
		lblIngresarUsuario.setFont(new Font("Segoe Print", Font.PLAIN, 20));
		lblIngresarUsuario.setBounds(83, 27, 173, 65);
		contentPane.add(lblIngresarUsuario);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Segoe Script", Font.PLAIN, 12));
		lblId.setBounds(22, 125, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setFont(new Font("Segoe Script", Font.PLAIN, 12));
		lblContrasea.setBounds(10, 156, 101, 35);
		contentPane.add(lblContrasea);
		
		txtId = new JTextField();
		txtId.setBounds(101, 122, 202, 20);
		contentPane.add(txtId);
		txtId.setColumns(10);
		
		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (txtId.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Ingrese ID");
					frmPrincipal.setVisible(false);
				}
				else if(txtContrase�a.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Ingrese Password");
					frmPrincipal.setVisible(false);
				}
				else frmPrincipal.setVisible(true);					
					JOptionPane.showMessageDialog(null, "Bienvenido");
					
					
				
			}
		});
		btnIngresar.setHideActionText(true);
		btnIngresar.setBorder(null);
		btnIngresar.setForeground(SystemColor.desktop);
		btnIngresar.setBackground(SystemColor.controlShadow);
		btnIngresar.setBounds(22, 212, 89, 36);
		contentPane.add(btnIngresar);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBackground(SystemColor.controlShadow);
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmRegistro.setVisible(true);
			}
		});
		btnRegistrar.setBounds(214, 212, 89, 36);
		contentPane.add(btnRegistrar);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBackground(SystemColor.controlShadow);
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(-1); //Para cerrar toda la aplicacion
			}
		});
		btnSalir.setBounds(123, 259, 89, 36);
		contentPane.add(btnSalir);
		
		txtContrase�a = new JPasswordField();
		txtContrase�a.setBounds(105, 163, 198, 20);
		contentPane.add(txtContrase�a);
	}
}
