package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EstudioRegistro extends JFrame {

	private JPanel contentPane;
	private JTable tableEmpresa;
	private JTable tableCotizacion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EstudioRegistro frame = new EstudioRegistro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EstudioRegistro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 572);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEstudioDeMercado = new JLabel("Estudio de Mercado");
		lblEstudioDeMercado.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstudioDeMercado.setFont(new Font("Segoe Print", Font.PLAIN, 15));
		lblEstudioDeMercado.setBounds(234, 11, 188, 54);
		contentPane.add(lblEstudioDeMercado);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 79, 384, 176);
		contentPane.add(scrollPane);
		
		tableEmpresa = new JTable();
		tableEmpresa.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"RUC", "Nombre", "Tipo"
			}
		));
		scrollPane.setViewportView(tableEmpresa);
		
		JLabel lblEmpresas = new JLabel("Empresas");
		lblEmpresas.setBounds(38, 54, 62, 14);
		contentPane.add(lblEmpresas);
		
		JButton btnEliminarEmpresa = new JButton("Eliminar Empresa");
		btnEliminarEmpresa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) tableEmpresa.getModel();
				int a = tableEmpresa.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminarEmpresa.setBounds(455, 92, 142, 46);
		contentPane.add(btnEliminarEmpresa);
		
		JButton btnBuscarEmpresa = new JButton("Buscar Empresa");
		btnBuscarEmpresa.setBounds(455, 165, 142, 46);
		contentPane.add(btnBuscarEmpresa);
		
		JLabel lblCotizaciones = new JLabel("Cotizaciones");
		lblCotizaciones.setBounds(38, 266, 87, 14);
		contentPane.add(lblCotizaciones);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(48, 291, 549, 130);
		contentPane.add(scrollPane_1);
		
		tableCotizacion = new JTable();
		tableCotizacion.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Numero", "Categoria", "Descripcion", "Ruc Empresa", "Precio", "Cantidad", "Fecha"
			}
		));
		tableCotizacion.getColumnModel().getColumn(0).setPreferredWidth(60);
		tableCotizacion.getColumnModel().getColumn(1).setPreferredWidth(70);
		tableCotizacion.getColumnModel().getColumn(2).setPreferredWidth(89);
		tableCotizacion.getColumnModel().getColumn(3).setPreferredWidth(86);
		tableCotizacion.getColumnModel().getColumn(4).setPreferredWidth(53);
		tableCotizacion.getColumnModel().getColumn(5).setPreferredWidth(59);
		tableCotizacion.getColumnModel().getColumn(6).setPreferredWidth(61);
		scrollPane_1.setViewportView(tableCotizacion);
		
		JButton btnEliminarCotizacion = new JButton("Eliminar Cotizacion");
		btnEliminarCotizacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) tableCotizacion.getModel();
				int a = tableCotizacion.getSelectedRow();
				if(a<0){
					JOptionPane.showMessageDialog(null, "Seleccione una Fila");
				}else {
					int confirmar = JOptionPane.showConfirmDialog(null, "Esta Seguro de Remmover esta Fila? ");
					if(JOptionPane.OK_OPTION == confirmar){
					model.removeRow(a);
					JOptionPane.showMessageDialog(null, "Registro Eliminado");
				}
				
				}
			}
		});
		btnEliminarCotizacion.setBounds(147, 445, 142, 46);
		contentPane.add(btnEliminarCotizacion);
		
		JButton btnBuscarCotizaxiones = new JButton("Buscar Cotizaciones");
		btnBuscarCotizaxiones.setBounds(387, 445, 159, 46);
		contentPane.add(btnBuscarCotizaxiones);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						btnRegresarCierra(e);
					}
					void btnRegresarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setBounds(535, 500, 89, 23);
		contentPane.add(btnRegresar);
	}

}
