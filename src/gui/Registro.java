package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

import javax.swing.SwingConstants;
import javax.swing.JPasswordField;

import controlador.ControladorUsuario;
import entidad.UsuarioBean;
import util.ConexionDB;

import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.Statement;


public class Registro extends JDialog {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtEmail;
	private JTextField txtTelefono;
	private JTextField txtDireccion;
	private JTextField txtEdad;
	private JTextField txtLogin;
	private JPasswordField txtPassword;
	private JComboBox cboCargo;
	private JComboBox cboSexo;
	private JTextField txtIdUsuario;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registro frame = new Registro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registro() {

		setBounds(100, 100, 378, 499);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegistroUsuario = new JLabel("Registro Usuario");
		lblRegistroUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistroUsuario.setFont(new Font("Segoe Script", Font.PLAIN, 18));
		lblRegistroUsuario.setBounds(28, 21, 316, 37);
		contentPane.add(lblRegistroUsuario);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(28, 102, 53, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(28, 133, 53, 14);
		contentPane.add(lblApellido);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(28, 158, 46, 23);
		contentPane.add(lblEmail);
		
		JLabel lblCargo = new JLabel("Cargo:");
		lblCargo.setBounds(28, 192, 46, 14);
		contentPane.add(lblCargo);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(28, 226, 53, 14);
		contentPane.add(lblTelefono);
		
		JLabel lblDireccion = new JLabel("Direccion:");
		lblDireccion.setBounds(28, 251, 64, 14);
		contentPane.add(lblDireccion);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setBounds(28, 276, 46, 14);
		contentPane.add(lblEdad);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(28, 301, 46, 14);
		contentPane.add(lblSexo);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(111, 99, 233, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setText("");
		txtApellido.setBounds(111, 130, 233, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(111, 159, 233, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setText("");
		txtTelefono.setBounds(111, 223, 150, 20);
		contentPane.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtDireccion = new JTextField();
		txtDireccion.setText("");
		txtDireccion.setBounds(111, 248, 233, 20);
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		txtEdad = new JTextField();
		txtEdad.setText("");
		txtEdad.setBounds(111, 273, 86, 20);
		contentPane.add(txtEdad);
		txtEdad.setColumns(10);
		
		final JComboBox cboCargo = new JComboBox();
		cboCargo.setModel(new DefaultComboBoxModel(new String[] {"Logistica", "Programacion", "Administrador"}));
		cboCargo.setBounds(111, 189, 172, 20);
		contentPane.add(cboCargo);
		
		final JComboBox cboSexo = new JComboBox();
		cboSexo.setModel(new DefaultComboBoxModel(new String[] {"Hombre", "Mujer", "Peluche"}));
		cboSexo.setBounds(111, 298, 150, 20);
		contentPane.add(cboSexo);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBackground(SystemColor.controlShadow);
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 UsuarioBean bean = new UsuarioBean();
				 bean.setIdUsuario(Integer.parseInt(txtIdUsuario.getText().trim()));
				 bean.setNombre(txtNombre.getText().trim());
				 bean.setApellido(txtApellido.getText().trim());
				 bean.setEmail(txtEmail.getText().trim());
			   //bean.setIdCargo(Integer.parseInt(null,cboCargo.getSelectedIndex()));
				 bean.setTelefono(txtTelefono.getText().trim());
				 bean.setDireccion(txtDireccion.getText().trim());
				 bean.setEdad(txtEdad.getText().trim());
				 bean.setSexo((String)cboSexo.getSelectedItem());
				 bean.setLogin(txtLogin.getText().trim());
				 bean.setContraseña(txtPassword.getSelectedText());
				 
				 ControladorUsuario u = new ControladorUsuario();
				 int salida = u.insertaUsuario(bean);
				 if(salida>0){
					 JOptionPane.showMessageDialog(null, "Registro Exitoso");
				 }else{
					 JOptionPane.showMessageDialog(null, "Error en el Registro");
				 }
					 
				validaLimpia();
			}	
		});
		btnRegistrar.setBounds(29, 410, 89, 23);
		contentPane.add(btnRegistrar);
		
		final JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(SystemColor.controlShadow);
		btnCancelar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				btnCancelar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						btnCancelarCierra(e);
					}
					void btnCancelarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnCancelar.setBounds(259, 410, 89, 23);
		contentPane.add(btnCancelar);
		
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setBounds(28, 336, 46, 14);
		contentPane.add(lblLogin);
		
		txtLogin = new JTextField();
		txtLogin.setToolTipText("");
		txtLogin.setBounds(111, 333, 86, 20);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setBounds(28, 365, 73, 14);
		contentPane.add(lblContrasea);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(111, 362, 85, 20);
		contentPane.add(txtPassword);
		
		JLabel lblIdusuario = new JLabel("idUsuario");
		lblIdusuario.setBounds(28, 69, 64, 14);
		contentPane.add(lblIdusuario);
		
		txtIdUsuario = new JTextField();
		txtIdUsuario.setBounds(111, 69, 86, 20);
		contentPane.add(txtIdUsuario);
		txtIdUsuario.setColumns(10);
		
		
	}
	public void validaLimpia(){
			if(txtNombre.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Ingrese Nombre");
			}
			else if(txtApellido.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Ingrese Apellido");
			}
			else if(txtEmail.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Ingrese E-Mail");
			}
			else if(cboCargo.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(null, "Seleccione Cargo");
			}
			else if(txtTelefono.getText().equals("1234567890#")){
				JOptionPane.showMessageDialog(null, "Ingrese Telefono");
			}
			else if(txtDireccion.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Ingrese Direccion");
			}
			else if(txtEdad.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Ingrese Edad");
			}
			else if(cboSexo.getSelectedIndex()==-1){
				JOptionPane.showMessageDialog(null, "Seleccione Sexo");
			}
			else JOptionPane.showMessageDialog(null, "Registro Exitoso!");
			txtNombre.setText("");
			txtApellido.setText("");
			txtEmail.setText("");
			cboCargo.setSelectedIndex(0);
			txtTelefono.setText("");
			txtDireccion.setText("");
			txtEdad.setText("");
			cboSexo.setSelectedIndex(0);
			txtNombre.transferFocusDownCycle();//Seleccciona el Foco |
		}
}
