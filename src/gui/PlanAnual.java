package gui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JPopupMenu;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import java.net.URI;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PlanAnual extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumeroPre;
	private JTextField txtFechaPre;
	private JTextField txtDescripcionPre;
	private JTextField txtCantidad;
	private JTextField txtCosto;
	private JTextField txtNumeroRD;
	private JTextField txtFechaRD;
	private JTextField txtDescripcionRD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlanAnual frame = new PlanAnual();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PlanAnual() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 488);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPlanAnual = new JLabel("Plan Anual");
		lblPlanAnual.setFont(new Font("Segoe Script", Font.PLAIN, 17));
		lblPlanAnual.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlanAnual.setBounds(184, 36, 160, 28);
		contentPane.add(lblPlanAnual);
		
		JLabel lblTechoPresupuestal = new JLabel("Techo Presupuestal");
		lblTechoPresupuestal.setBounds(10, 91, 135, 14);
		contentPane.add(lblTechoPresupuestal);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setBounds(30, 116, 46, 14);
		contentPane.add(lblNumero);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(30, 145, 46, 14);
		contentPane.add(lblFecha);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(30, 170, 68, 14);
		contentPane.add(lblDescripcion);
		
		JLabel lblCantidad = new JLabel("Cantidad");
		lblCantidad.setBounds(30, 200, 68, 14);
		contentPane.add(lblCantidad);
		
		JLabel lblCostoTotal = new JLabel("Costo Total");
		lblCostoTotal.setBounds(30, 227, 68, 14);
		contentPane.add(lblCostoTotal);
		
		txtNumeroPre = new JTextField();
		txtNumeroPre.setBounds(105, 116, 135, 20);
		contentPane.add(txtNumeroPre);
		txtNumeroPre.setColumns(10);
		
		txtFechaPre = new JTextField();
		txtFechaPre.setBounds(105, 142, 86, 20);
		contentPane.add(txtFechaPre);
		txtFechaPre.setColumns(10);
		
		txtDescripcionPre = new JTextField();
		txtDescripcionPre.setBounds(105, 167, 135, 20);
		contentPane.add(txtDescripcionPre);
		txtDescripcionPre.setColumns(10);
		
		txtCantidad = new JTextField();
		txtCantidad.setBounds(105, 197, 86, 20);
		contentPane.add(txtCantidad);
		txtCantidad.setColumns(10);
		
		txtCosto = new JTextField();
		txtCosto.setText("");
		txtCosto.setBounds(105, 224, 86, 20);
		contentPane.add(txtCosto);
		txtCosto.setColumns(10);
		
		JLabel lblEnvioDeRequerimiento = new JLabel("Envio de Requerimiento RD");
		lblEnvioDeRequerimiento.setBounds(286, 91, 182, 14);
		contentPane.add(lblEnvioDeRequerimiento);
		
		JLabel label = new JLabel("Numero");
		label.setBounds(298, 116, 46, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Fecha");
		label_1.setBounds(298, 145, 46, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Descripcion");
		label_2.setBounds(298, 170, 68, 14);
		contentPane.add(label_2);
		
		txtNumeroRD = new JTextField();
		txtNumeroRD.setBounds(382, 113, 86, 20);
		contentPane.add(txtNumeroRD);
		txtNumeroRD.setColumns(10);
		
		txtFechaRD = new JTextField();
		txtFechaRD.setBounds(382, 142, 86, 20);
		contentPane.add(txtFechaRD);
		txtFechaRD.setColumns(10);
		
		txtDescripcionRD = new JTextField();
		txtDescripcionRD.setBounds(382, 170, 135, 20);
		contentPane.add(txtDescripcionRD);
		txtDescripcionRD.setColumns(10);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(361, 223, 89, 23);
		contentPane.add(btnEnviar);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(102, 288, 89, 23);
		contentPane.add(btnRegistrar);
		
		final JLabel lblRegistrarPaacEn = new JLabel("Registrar PAAC en el SEASE");
		lblRegistrarPaacEn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblRegistrarPaacEn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				lblRegistrarPaacEn.setText("");
					try {
						if (Desktop.isDesktopSupported()) {
						Desktop desktop = Desktop.getDesktop();
						if (desktop.isSupported(Desktop.Action.BROWSE)) {
						desktop.browse(new URI("http://www.google.com"));
						}
						}
						} catch (Exception e) {
						e.printStackTrace();
						}
			}
		});
		lblRegistrarPaacEn.setFont(new Font("Segoe Print", Font.PLAIN, 13));
		lblRegistrarPaacEn.setBounds(30, 381, 210, 28);
		contentPane.add(lblRegistrarPaacEn);
		
		final JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRegresar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						btnCancelarCierra(e);
					}
					void btnCancelarCierra(ActionEvent e){
						dispose();
					}
				});
			}
		});
		btnRegresar.setBounds(428, 385, 89, 23);
		contentPane.add(btnRegresar);
	}

}
