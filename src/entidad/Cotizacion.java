package entidad;

public class Cotizacion {
	private int idCotizacion;
	private String cate;
	private String decrip;
	private double precio;
	private int canti;
	private String fecha;
	private int idEmpresa;
	public Cotizacion(int idCotizacion, String cate, String decrip,
			double precio, int canti, String fecha, int idEmpresa) {
		super();
		this.idCotizacion = idCotizacion;
		this.cate = cate;
		this.decrip = decrip;
		this.precio = precio;
		this.canti = canti;
		this.fecha = fecha;
		this.idEmpresa = idEmpresa;
	}
	public Cotizacion() {
	}
	public int getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(int idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public String getCate() {
		return cate;
	}
	public void setCate(String cate) {
		this.cate = cate;
	}
	public String getDecrip() {
		return decrip;
	}
	public void setDecrip(String decrip) {
		this.decrip = decrip;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getCanti() {
		return canti;
	}
	public void setCanti(int canti) {
		this.canti = canti;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
	
}