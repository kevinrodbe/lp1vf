package entidad;

import java.util.Date;

public class CuadroBean {
	private Integer idnumero;
	private String tipo, descripcion,presupuesto;
	private Date fecha;
	public Integer getIdnumero() {
		return idnumero;
	}
	public void setIdnumero(Integer idnumero) {
		this.idnumero = idnumero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getPresupuesto() {
		return presupuesto;
	}
	public void setPresupuesto(String presupuesto) {
		this.presupuesto = presupuesto;
	}
	
}
