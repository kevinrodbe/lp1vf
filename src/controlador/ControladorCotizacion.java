package controlador;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.ConexionDB;
import entidad.Cotizacion;
import entidad.CuadroBean;
import entidad.UsuarioBean;

public class ControladorCotizacion {
	public List<Cotizacion>getCotizacion(){
		List<Cotizacion>lista= new ArrayList<Cotizacion>();
		Connection conn= null;
		PreparedStatement pstm = null;
		ResultSet rs= null;
		try{
			String sql= " Select c.idCotizacion "
					+ " from cotizacion c ";			
			conn = new ConexionDB().getConexion();
			pstm = conn.prepareStatement(sql);
			rs=pstm.executeQuery();
			
			while(rs.next()){
				Cotizacion objC= new Cotizacion( 
					rs.getInt("idCotizacion"), 
					rs.getString("categoria"), 
					rs.getString("descripcion"), 
					rs.getDouble("precio"),
					rs.getInt("cantidad"),
					rs.getString("fecha"),
					rs.getInt("idEmpresa"));
				lista.add(objC);
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!= null){	rs.close();		}
				if(pstm!= null){rs.close();}
				if(conn!=null){conn.close();}
				
			}catch (SQLException e){
				e.printStackTrace();
			}
		}
		return lista;
	}
}
