package controlador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import util.ConexionDB;
import entidad.EmpresaBean;
public class ControladorEmpresa {
	public int insertaEmpresa(EmpresaBean bean){
		int salida = -1;
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = new ConexionDB().getConexion();
			String sql = "insert into Empresa values(null,?,?,?)";
			pstm = conn.prepareStatement(sql);
			pstm.setString(1, bean.getRuc());
			pstm.setString(2, bean.getNombre());
			pstm.setString(3, bean.getTipo());
	
			
			salida = pstm.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstm != null)
					pstm.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				
			}
		}
		return salida;
	}

}
