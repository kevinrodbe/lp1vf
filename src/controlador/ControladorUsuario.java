package controlador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.ConexionDB;
import entidad.UsuarioBean;

public class ControladorUsuario {
	public int insertaUsuario(UsuarioBean bean){
		int salida = -1;
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = new ConexionDB().getConexion();
			String sql = "insert into Usuario values(null,?,?,?,?,?,?,?,?,?,?,?)";
			pstm = conn.prepareStatement(sql);
			pstm.setInt(1, bean.getIdUsuario());
			pstm.setString(2, bean.getNombre());
			pstm.setString(3, bean.getApellido());
			pstm.setString(4, bean.getEmail());
			pstm.setString(5, bean.getTelefono());
			pstm.setString(7, bean.getDireccion());
			pstm.setString(6, bean.getEdad());
			pstm.setString(8, bean.getSexo());
			pstm.setInt(9, bean.getIdCargo());
			pstm.setString(10, bean.getLogin());
			pstm.setString(11, bean.getContraseña());
			
			salida = pstm.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstm != null)
					pstm.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				
			}
		}
		return salida;
	}
	/*public ArrayList<UsuarioBean> lista(){
		
		ArrayList<UsuarioBean> data = new ArrayList<UsuarioBean>();
		UsuarioBean bean = null;
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = new ConexionDB().getConexion();
			String sql ="select * from Usuario";
			pstm = conn.prepareStatement(sql);
			ResultSet rs = pstm.executeQuery();
			
			while(rs.next()){
				bean = new UsuarioBean();
				 bean.setIdUsuario(rs.getInt("idUsuario"));
				 bean.setDirec(rs.getString("Direccion_Ejecutiva"));
				 bean.setUni(rs.getString("Unidad_Organica"));
				 bean.setProp(rs.getString("Proposito"));
				 bean.setAlcan(rs.getString("Alcance"));
				 bean.setMarco(rs.getString("Marco_Legal"));
				data.add(bean);
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally{
			try {
				conn.close();
				pstm.close();
			} catch (SQLException e) {
			}
		}
		return data;
	}*/

}
