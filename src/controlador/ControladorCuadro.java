package controlador;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import util.ConexionDB;
import entidad.CuadroBean;
import entidad.UsuarioBean;

public class ControladorCuadro {
	public int insertaCuadro(CuadroBean bean){
		int salida = -1;
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = new ConexionDB().getConexion();
			String sql = "insert into Cuadro_necesidades values(null,?,?,null,?,?)";
			pstm = conn.prepareStatement(sql);			
			pstm.setString(1, bean.getTipo());
			pstm.setString(2, bean.getDescripcion());
			//pstm.setDate(3, (Date) bean.getFecha());
			pstm.setString(3, bean.getPresupuesto());
			pstm.setInt(4, bean.getIdnumero());
			
			salida = pstm.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstm != null)
					pstm.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {
				
			}
		}
		return salida;
	}
}
