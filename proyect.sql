SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Empresa` (
  `idEmpresa` INT NOT NULL AUTO_INCREMENT,
  `Ruc` VARCHAR(45) NULL,
  `Nombre` VARCHAR(45) NULL,
  `Tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`idEmpresa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cotizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cotizacion` (
  `idCotizacion` INT NOT NULL AUTO_INCREMENT,
  `categoria` VARCHAR(45) NULL,
  `descripcion` VARCHAR(45) NULL,
  `precio` VARCHAR(45) NULL,
  `cantidad` VARCHAR(45) NULL,
  `fecha` DATE NULL,
  `idEmpresa` INT NOT NULL,
  PRIMARY KEY (`idCotizacion`),
  INDEX `fk_Cotizacion_Empresa_idx` (`idEmpresa` ASC),
  CONSTRAINT `fk_Cotizacion_Empresa`
    FOREIGN KEY (`idEmpresa`)
    REFERENCES `mydb`.`Empresa` (`idEmpresa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cuadro_Necesidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cuadro_Necesidades` (
  `idCuadro_Necesidades` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  `descripcion` VARCHAR(45) NULL,
  `fecha` DATE NULL,
  `Presupuesto` VARCHAR(45) NULL,
  `Cotizacion_idCotizacion` INT NOT NULL,
  PRIMARY KEY (`idCuadro_Necesidades`),
  INDEX `fk_Cuadro_Necesidades_Cotizacion1_idx` (`Cotizacion_idCotizacion` ASC),
  CONSTRAINT `fk_Cuadro_Necesidades_Cotizacion1`
    FOREIGN KEY (`Cotizacion_idCotizacion`)
    REFERENCES `mydb`.`Cotizacion` (`idCotizacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`RD`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`RD` (
  `idRD` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NULL,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idRD`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Techos_Presupuestales`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Techos_Presupuestales` (
  `idTechos_Presupuestales` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NULL,
  `descripcion` VARCHAR(45) NULL,
  `cantidad` VARCHAR(45) NULL,
  `costototal` VARCHAR(45) NULL,
  `Cotizacion_idCotizacion` INT NOT NULL,
  `RD_idRD` INT NOT NULL,
  PRIMARY KEY (`idTechos_Presupuestales`),
  INDEX `fk_Techos_Presupuestales_Cotizacion1_idx` (`Cotizacion_idCotizacion` ASC),
  INDEX `fk_Techos_Presupuestales_RD1_idx` (`RD_idRD` ASC),
  CONSTRAINT `fk_Techos_Presupuestales_Cotizacion1`
    FOREIGN KEY (`Cotizacion_idCotizacion`)
    REFERENCES `mydb`.`Cotizacion` (`idCotizacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Techos_Presupuestales_RD1`
    FOREIGN KEY (`RD_idRD`)
    REFERENCES `mydb`.`RD` (`idRD`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cargo` (
  `idCargo` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idCargo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `apellido` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `idCargo` INT NOT NULL,
  `telefono` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  `edad` VARCHAR(45) NULL,
  `sexo` VARCHAR(45) NULL,
  `login` VARCHAR(45) NULL,
  `contraseña` VARCHAR(45) NULL,
  `idCuadro_Necesidades` INT NOT NULL,
  `idTechos_Presupuestales` INT NOT NULL,
  PRIMARY KEY (`idUsuario`),
  INDEX `fk_Usuario_Cuadro_Necesidades1_idx` (`idCuadro_Necesidades` ASC),
  INDEX `fk_Usuario_Techos_Presupuestales1_idx` (`idTechos_Presupuestales` ASC),
  INDEX `fk_Usuario_Cargo1_idx` (`idCargo` ASC),
  CONSTRAINT `fk_Usuario_Cuadro_Necesidades1`
    FOREIGN KEY (`idCuadro_Necesidades`)
    REFERENCES `mydb`.`Cuadro_Necesidades` (`idCuadro_Necesidades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Techos_Presupuestales1`
    FOREIGN KEY (`idTechos_Presupuestales`)
    REFERENCES `mydb`.`Techos_Presupuestales` (`idTechos_Presupuestales`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Cargo1`
    FOREIGN KEY (`idCargo`)
    REFERENCES `mydb`.`Cargo` (`idCargo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
